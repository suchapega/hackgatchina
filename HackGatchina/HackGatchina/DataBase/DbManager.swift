//
//  DbManager.swift
//  HackGatchina
//
//  Created by Arsenii on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import Firebase
import UIKit
import FirebaseStorage

class DbManager {
    
    static let shared = DbManager()
    
    private lazy var db = Firestore.firestore()
    
    private func taskConvertation(document: QueryDocumentSnapshot) -> Task {
    
        let tempData = document.data()
        
        let images = tempData["images"] as! Array<String>
        let text = tempData["text"] as! String
        let address = tempData["address"] as! String
        let position = tempData["position"] as! GeoPoint
        let emotionalTextColor = TextColoring(rawValue: tempData["textEmotional"] as! Int)
        let status = TaskStatus(rawValue: tempData["status"] as! Int)
        let rating = tempData["rating"] as! Int
        
        let date = (tempData["date"] as! Firebase.Timestamp).dateValue()
        
        let tags = tempData["tags"] as! Array<String>
        
        let tempTask = Task(text: text, address: address, position: position, textEmotional: emotionalTextColor!, status: status!, rating: rating, tags: Set(tags), date: date)
        
        tempTask.addPhotos(images: images)
        
        return tempTask;
    }
    
    func addNewTask(task: Task) {
        var ref: DocumentReference? = nil
        ref = db.collection("tasks").addDocument(data: [
            "text": task.text,
            "images": Array(task.images),
            "address": task.address,
            "position": task.position,
            "textEmotional": task.textEmotional.rawValue,
            "status": task.status.rawValue,
            "rating": task.rating,
            "date": task.date,
            "tags": Array(task.tags)
            
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    //todo fix not nullable types
    func loadTasks(completion: @escaping ([Task]) -> Void) {

        db.collection("tasks")
            .order(by: "date", descending: true)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    var tasks = [Task]()
                    for document in querySnapshot!.documents {
                        tasks.append(self.taskConvertation(document: document))
                        //print("\(document.documentID) => \(document.data())")
                    }
                    completion(tasks)
                }
        }
    }
    
    private var listener: ListenerRegistration?
    
    func subscribeToUpdates(dbListener: @escaping ([Task]) -> Void) {
        listener = db.collection("tasks")
            .order(by: "date", descending: true)
            .addSnapshotListener { snapshot, error in
                guard let documents = snapshot?.documents else {
                    print("Error fetching document: \(error!)")
                    return
                }
                
                var tasks = [Task]()
                
                for document in documents {
                    tasks.append(self.taskConvertation(document: document))
                }
                
                dbListener(tasks)
        }
    }
    
    func unsubscribe() {
        listener?.remove()
    }
    
    private func LoadImageToServer(_ image: UIImage, completion: @escaping (String?) -> Void) {
        
        let storage = Storage.storage()
        var data = image.pngData()!
        // Create a storage reference from our storage service
        let storageRef = storage.reference()

        // Create a reference to the file you want to upload
        let riversRef = storageRef.child(NSUUID().uuidString + ".png")
        
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.putData(data, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                return
            }
            // Metadata contains file metadata such as size, content-type.
            let size = metadata.size
            // You can also access to download URL after upload.
            storageRef.downloadURL { (url, error) in
                guard error != nil, let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }
                
                completion(downloadURL.absoluteString)
            }
        }
    }
}
