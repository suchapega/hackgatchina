//
//  LocationExtention.swift
//  HackGatchina
//
//  Created by Arsenii on 26/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        // create CLLocation from the coordinates of CLVisit
        let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)
        
        // Get location description
    }
    
    func newVisitReceived(_ visit: CLVisit, description: String) {
        let location = Location(visit: visit, descriptionString: description)
        
        // Save location to disk
    }
}
