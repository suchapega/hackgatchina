//
//  Task+UI.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 26/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import UIKit

extension TextColoring {
    var uiColor: UIColor? {
        switch self {
        case .Neutral:
            return UIColor(hexRGB: "06205E")
        case .Positive:
            return UIColor(hexRGB: "4CD964")
        case .Negative:
            return UIColor(hexRGB: "FF5E00")
        case .Critical:
            return UIColor(hexRGB: "FF291D")
        }
    }

    var localizedDescription: String {
        switch self {
        case .Neutral:
            return "Нейтральное"
        case .Positive:
            return "Положительное"
        case .Negative:
            return "Негативное"
        case .Critical:
            return "Критичное"
        }
    }
}

extension TaskStatus {
    var uiImage: UIImage? {
        switch self {
        case .Moderation:
            return UIImage(named: "waiting")
        case .InWork:
            return UIImage(named: "in_process")
        case .Complite:
            return UIImage(named: "done")
        case .Reject:
            return UIImage(named: "rejected")
        }
    }
}
