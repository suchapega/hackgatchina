//
//  UIColor+Hex.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init?(hexRGBA: String?) {
        guard let rgba = hexRGBA, let val = Int(rgba.replacingOccurrences(of: "#", with: ""), radix: 16) else {
            return nil
        }
        let redValue = CGFloat((val >> 24) & 0xff) / 255.0
        let greenValue = CGFloat((val >> 16) & 0xff) / 255.0
        let blueValue = CGFloat((val >> 8) & 0xff) / 255.0
        let alphaValue = CGFloat(val & 0xff) / 255.0
        self.init(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
    }

    convenience init?(hexRGB: String?) {
        guard let rgb = hexRGB else {
            return nil
        }
        self.init(hexRGBA: rgb + "ff")
    }
}
