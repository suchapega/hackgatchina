//
//  UIViewController+Helpers.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 26/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String?, message: String?, actionTitle: String = "OK", action: (() -> Void)? = nil) {
        let okAction = UIAlertAction(title: actionTitle, style: .cancel) { _ in
            action?()
        }
        showAlert(title: title, message: message, actions: okAction)
    }

    fileprivate func showAlert(title: String?, message: String?, actions: UIAlertAction...) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
}
