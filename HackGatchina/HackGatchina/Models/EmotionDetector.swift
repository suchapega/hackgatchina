//
//  Dictionary.swift
//  HackGatchina
//
//  Created by Arsenii on 26/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation

class EmotionDetector {
    
    static let possitiveWords = ["спасибо", "благодарим", "признательны", "рады"]
    static let negativeWords = ["жалоба", "нашел", "мешает", "мусор", "яма", "снег", "сугробы"]
    static let criticalWords = ["невыносимо", "ужасно", "плохо", "безумная"]
    
    static func detectEmotions(_ text: String) -> TextColoring {
        
        var analazeredText = text.lowercased()
        
        for possitiveWord in possitiveWords {
            if analazeredText.contains(possitiveWord) {
                return .Positive
            }
        }
        
        for negativeWord in negativeWords {
            if analazeredText.contains(negativeWord) {
                return .Negative
            }
        }
        
        for criticalWord in criticalWords {
            if analazeredText.contains(criticalWord) {
                return .Critical
            }
        }
        
        return .Neutral
    }
}
