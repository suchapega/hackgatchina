//
//  TagsDetector.swift
//  HackGatchina
//
//  Created by Arsenii on 26/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation

struct Tag {
    var value: String
    var keyWords: [String]
}

class TagsDetector {
    
    static var constTags = [Tag(value : "Благоустройство",
                                keyWords: ["благрустройство", "дом", "трава", "газон", "площадка", "подъезд", "парк"]),
                       Tag(value : "ЖКХ",
                           keyWords: ["жкх", "труба", "вода", "отопление", "лестница", "лифт"]),
                       Tag(value : "Дороги",
                           keyWords: ["яма", "дорога", "улица", "фонарь", "знак", "светофор", "авария","машина","стоянка","угол","угла"]),
                       Tag(value : "Уборка",
                           keyWords: ["уборка", "мусор", "грязь", "свалка", "контейнер", "территория", "листья","лужа","песок","снег", "сугроб"]),
                       Tag(value : "Торговля",
                           keyWords: ["магазин", "ТЦ", "ТК", "ТРК", "продавец", "разрешение", "коммерческая"]),
                       Tag(value : "Культура",
                           keyWords: ["мероприятие", "субботник", "праздник", "флаг", "концерт"]),
                       Tag(value : "Образование",
                           keyWords: ["образование", "школа", "садик", "гимназия", "ГОУ", "СОШ", "СДЮШОР", "ФОК"]),
                       Tag(value : "Спорт",
                           keyWords: ["спорт", "СДЮШОР", "ФОК", "бассейн", "стадион", "турник", "площадка","зал"]),
                       Tag(value : "Администрация",
                           keyWords: ["администрация", "жалоба", "работа", "прием"]),
                       Tag(value : "Благодарности",
                           keyWords: ["спасибо", "благодарим", "признательны", "рады"]),
                       Tag(value : "Парки",
                           keyWords: ["парк", "деревья", "дерево", "лес", "пруд", "белка"]),
                       Tag(value : "Животные",
                           keyWords: ["собака", "кошка", "крыса", "белка"])
                    ]
    
    static func detectTags(_ text: String) -> [String] {
        
        var tags = Array<String>()
        let analazeredText = text.lowercased()
        
        for constTag in constTags {
            for keyWord in constTag.keyWords {
                if (analazeredText.contains(keyWord)){
                    tags.append(constTag.value)
                    break
                }
            }
        }
        
        return tags
    }
}
