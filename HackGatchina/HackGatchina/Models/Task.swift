//
//  Task.swift
//  HackGatchina
//
//  Created by Arsenii on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage

enum TaskStatus: Int {
    case Moderation = 0
    case InWork = 1
    case Complite = 2
    case Reject = 3
}

enum TextColoring: Int {
    case Neutral = 0
    case Positive = 1
    case Negative = 2
    case Critical = 3
}

class Task {
    private(set) var text: String
    private(set) var images: [String]
    private(set) var address: String?
    private(set) var position: GeoPoint?
    private(set) var textEmotional: TextColoring
    private(set) var status: TaskStatus
    private(set) var rating: Int
    private(set) var tags: Set<String>
    private(set) var date: Date
    
    init(_ text: String) {
        self.text = text
        images = [String]()
        address = "пр. Медиков, 3 лит. А"
        position = GeoPoint(latitude: 0.0, longitude: 0.0)
        textEmotional = .Neutral
        status = .Moderation
        rating = 0
        tags = Set()
        date = Date()
    }
    
    init(text: String, address: String, position: GeoPoint, textEmotional: TextColoring, status: TaskStatus, rating: Int, tags: Set<String>, date: Date) {
        self.text = text
        self.images = [String]()
        self.address = address
        self.position = position
        self.textEmotional = textEmotional
        self.status = status
        self.rating = rating
        self.tags = tags
        self.date = date
    }
    
    func addPhotos(images: [String]) {
        self.images.append(contentsOf: images)
    }

    func addPhoto(image: String) {
        images.append(image)
    }

    func setAddress(address: String) {
        self.address = address
    }

    func setPosition(position: GeoPoint) {
        self.position = position
    }

    func setStatus(status: TaskStatus) {
        self.status = status
    }

    static func recognizeTextColor(_ text: String) -> TextColoring {
        return EmotionDetector.detectEmotions(text)
    }

    func recognizeTextColor() {
        textEmotional = EmotionDetector.detectEmotions(text)
    }
    
    static func recognizeTags(_ text: String) -> Set<String> {
        return Set(TagsDetector.detectTags(text))
    }

    func recognizeTags() {
        tags = Task.recognizeTags(text)
    }
    
    func addTag(tag: String) {
        tags.insert(tag)
    }

    func removeTag(tag: String) {
        tags.remove(tag)
    }

    func setTags(tags: [String]) {
        self.tags = Set(tags)
    }

    func increaseRating() {
        rating += 1
    }

    func decreaseRating() {
        rating -= 1
    }
}
