//
//  MainViewController.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit
import TagListView
import MapKit
import CoreLocation

class MainViewController: UIViewController, UITextViewDelegate, TagListViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var emptyTextView: UITextView!
    @IBOutlet private weak var taskTextView: UITextView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var sendButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tagsListView: TagListView!
    @IBOutlet private weak var addTagButton: UIButton!
    @IBOutlet private weak var photoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        taskTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 16 + 48 /*tags height*/, right: 16)
        taskTextView.delegate = self
        sendButton.layer.cornerRadius = 8
        addTagButton.layer.cornerRadius = 8

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        tagsListView.textFont = UIFont.systemFont(ofSize: 13, weight: .semibold)
        tagsListView.delegate = self

        photoImageView.layer.cornerRadius = 8
        photoImageView.layer.borderWidth = 1
        photoImageView.layer.borderColor = TextColoring.Neutral.uiColor?.cgColor
    }

    var locationManager:CLLocationManager!
    
    override func viewWillAppear(_ animated: Bool) {
        taskTextView.becomeFirstResponder()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        //requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }

        let keyboardHeight = keyboardFrame.cgRectValue.height
        sendButtonBottomConstraint.constant = keyboardHeight + 8 - view.safeAreaInsets.bottom
        view.layoutIfNeeded()
    }

    @objc func keyboardWillHide(notification: NSNotification){
        sendButtonBottomConstraint.constant = 8
        view.layoutIfNeeded()
    }

    var lastRecognizedText = ""
    func textViewDidChange(_ textView: UITextView) {
        let trimmedText = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)

        emptyTextView.isHidden = !trimmedText.isEmpty
        addTagButton.isHidden = trimmedText.isEmpty

        if trimmedText.isEmpty {
            tagsListView.removeAllTags()
            sendButton.backgroundColor = TextColoring.Neutral.uiColor
        } else if trimmedText != lastRecognizedText {
            let textEmotional = Task.recognizeTextColor(trimmedText)
            let tags = Task.recognizeTags(trimmedText)

            UIView.animate(withDuration: 0.23) {
                self.sendButton.backgroundColor = textEmotional.uiColor
            }
            tagsListView.removeAllTags()
            tagsListView.addTags(Array(tags))
            lastRecognizedText = trimmedText
        }
    }

    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagsListView.removeTag(title)
    }

    @IBAction func editAddressAction(_ sender: Any) {
    }
    
    @IBAction func addTagAction(_ sender: Any) {
    }

    @IBAction func sendAction(_ sender: Any) {
        let trimmedText = taskTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !trimmedText.isEmpty else {
            showAlert(title: "¯\\_(ツ)_/¯", message: "Для отправки отклика необходимо добавить описание")
            return
        }

        let task = Task(trimmedText)
        task.recognizeTags()
        task.recognizeTextColor()
        DbManager.shared.addNewTask(task: task)

        taskTextView.text = ""
        emptyTextView.isHidden = !taskTextView.text.isEmpty
        addTagButton.isHidden = taskTextView.text.isEmpty
        tagsListView.removeAllTags()
        sendButton.backgroundColor = TextColoring.Neutral.uiColor
        goToList()
    }

    @IBAction func photoAction(_ sender: Any) {
        
        let camera = DSCameraHandler(delegate_: self)
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        optionMenu.popoverPresentationController?.sourceView = self.view
        
        let takePhoto = UIAlertAction(title: "Камера", style: .default) { (alert : UIAlertAction!) in
            camera.getCameraOn(self, canEdit: false)
        }
        let sharePhoto = UIAlertAction(title: "Галарея", style: .default) { (alert : UIAlertAction!) in
            camera.getPhotoLibraryOn(self, canEdit: false)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (alert : UIAlertAction!) in
        }
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let image = info["UIImagePickerControllerEditedImage"] as! UIImage
        photoImageView.image = image
        photoImageView.isHidden = false
        // TODO
        // and send to backend

//        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func listAction(_ sender: Any) {
        goToList()
    }

    func goToList() {
        let listVC = ListViewController()
        let listNC = UINavigationController(rootViewController: listVC)
        listNC.navigationItem.largeTitleDisplayMode = .automatic
        listNC.navigationBar.prefersLargeTitles = true
        self.navigationController?.present(listNC, animated: true, completion: nil)
    }
}
