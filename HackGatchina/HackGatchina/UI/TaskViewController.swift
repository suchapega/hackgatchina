//
//  TaskViewController.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }

    func setupNavBar() {
        self.navigationItem.largeTitleDisplayMode = .never
    }
}
