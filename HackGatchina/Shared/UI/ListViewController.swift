//
//  CollectionViewControllerViewController.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit

enum AppTarget: String {
    case Citizens = "HackGatchina"
    case Administration = "HackGatchina.Admin"
}

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet private weak var tasksTableView: UITableView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    private let taskCellIdentifier = "TaskCell"
    lazy var target: AppTarget = {
        return AppTarget(rawValue: Bundle.main.infoDictionary!["TargetName"] as! String)!
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()

        tasksTableView.tableFooterView = UIView()
        tasksTableView.delegate = self
        tasksTableView.dataSource = self
        tasksTableView.register(UINib(nibName: taskCellIdentifier, bundle: nil), forCellReuseIdentifier: taskCellIdentifier)
        
        DbManager.shared.subscribeToUpdates(dbListener: dbListenerUpdate)
    }

    override func viewWillAppear(_ animated: Bool) {
        if tasks.isEmpty {
            activityIndicatorView.isHidden = false
        } else {
            tasksTableView.reloadData()
        }
    }

    var tasks = [Task]()
    private func dbListenerUpdate(tasks: [Task]) {
        self.tasks = tasks
        activityIndicatorView.isHidden = true
        tasksTableView.reloadData()
    }

    @objc func closeAction(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func mapAction(sender: UIBarButtonItem) {

    }

    @objc func filterAction(sender: UIBarButtonItem) {

    }

    func setupNavBar() {
        switch target {
        case .Citizens:
            let closeButton = UIBarButtonItem(image: UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(closeAction))
            self.navigationItem.leftBarButtonItem = closeButton
            self.navigationItem.title = "Отклики"
        case .Administration:
            self.navigationItem.title = "Обращения"
        }

        let mapButton = UIBarButtonItem(image: UIImage(named: "map")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(mapAction))
        let filterButton = UIBarButtonItem(image: UIImage(named: "filter")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(filterAction))
        self.navigationItem.rightBarButtonItems = [filterButton, mapButton]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: taskCellIdentifier, for: indexPath) as? TaskCell else {
            return UITableViewCell(frame: CGRect.zero)
        }

        cell.setupWith(task: tasks[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let taskVC = TaskViewController()
//        self.navigationController?.pushViewController(taskVC, animated: true)
    }
}
