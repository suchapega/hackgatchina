//
//  TableViewCell.swift
//  HackGatchina
//
//  Created by Pavel Chupryna + on 25/05/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit
import TagListView

class TaskCell: UITableViewCell {
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var statusImageView: UIImageView!
    @IBOutlet private weak var taskTextLabel: UILabel!
    @IBOutlet private weak var tagsListView: TagListView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        tagsListView.textFont = UIFont.systemFont(ofSize: 13, weight: .semibold)
    }

    func setupWith(task: Task) {
        addressLabel.text = task.address
        statusImageView.image = task.status.uiImage
        taskTextLabel.text = task.text

        tagsListView.removeAllTags()
        let typeTag = tagsListView.addTag(task.textEmotional.localizedDescription)
        typeTag.tagBackgroundColor = task.textEmotional.uiColor!
        typeTag.textColor = UIColor.white

        tagsListView.addTags(Array(task.tags))

        let df = DateFormatter()
        df.dateFormat = "dd MMM yyyy"
        df.locale = Locale(identifier: "ru")
        dateLabel.text = df.string(from: task.date)

        let ratingSign = task.rating > 0 ? "+" : ""
        ratingLabel.text = ratingSign + String(task.rating)

        contentView.backgroundColor = (task.status == .Complite) ? UIColor(hexRGB: "F9F9F9") : UIColor.white
    }
    
}
