# Gatchina.Life
![icon](Images/Icon.png)

#### iOS app for simplifying sending reports to city administration
It was developed in "HackGatchina" hackaton

##### Technology stack

* Filebase Cloud Firestore for saving reports and realtime update on several devices (we have two apps for reporters and for admins)
* CoreLocation for detecting current location
* Algorithm for automatic identification of report mood (changing button color) and categories (tags) for sending report to the correct city department

|Focused only on entering repport information, without any selections|Parsing text to needed categories and detecting mood|
|-|-|
|![Enter report by voice](Images/1.png)|![Enter report by text](Images/2.png)|

|List of all reports with filters, status, categories and map view|And more details about each report|
|-|-|
|![Enter report by voice](Images/3.png)|![Enter report by text](Images/4.png)|

##### Prototype
![Sync prototype](gatchina-life.gif)